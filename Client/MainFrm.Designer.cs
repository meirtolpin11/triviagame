﻿namespace WindowsFormsApplication3
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.signout = new System.Windows.Forms.Button();
            this.signup = new System.Windows.Forms.Button();
            this.createroom = new System.Windows.Forms.Button();
            this.bestscore = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.joinroom = new System.Windows.Forms.Button();
            this.mystatus = new System.Windows.Forms.Button();
            this.signIn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // signout
            // 
            this.signout.BackColor = System.Drawing.Color.Transparent;
            this.signout.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.SignOut_01_01_01;
            this.signout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.signout.Location = new System.Drawing.Point(183, 126);
            this.signout.Name = "signout";
            this.signout.Size = new System.Drawing.Size(79, 75);
            this.signout.TabIndex = 0;
            this.signout.UseVisualStyleBackColor = false;
            this.signout.Click += new System.EventHandler(this.signout_Click);
            // 
            // signup
            // 
            this.signup.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.signup_01_01_01;
            this.signup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.signup.Location = new System.Drawing.Point(265, 126);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(79, 75);
            this.signup.TabIndex = 1;
            this.signup.UseVisualStyleBackColor = true;
            this.signup.Click += new System.EventHandler(this.signup_Click);
            // 
            // createroom
            // 
            this.createroom.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.createRoom_01;
            this.createroom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.createroom.Location = new System.Drawing.Point(265, 202);
            this.createroom.Name = "createroom";
            this.createroom.Size = new System.Drawing.Size(161, 75);
            this.createroom.TabIndex = 2;
            this.createroom.UseVisualStyleBackColor = true;
            this.createroom.Click += new System.EventHandler(this.createroom_Click);
            // 
            // bestscore
            // 
            this.bestscore.BackColor = System.Drawing.Color.Transparent;
            this.bestscore.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.bestScore_01;
            this.bestscore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bestscore.Location = new System.Drawing.Point(184, 279);
            this.bestscore.Name = "bestscore";
            this.bestscore.Size = new System.Drawing.Size(162, 75);
            this.bestscore.TabIndex = 3;
            this.bestscore.UseVisualStyleBackColor = false;
            this.bestscore.Click += new System.EventHandler(this.bestscore_Click);
            // 
            // exit
            // 
            this.exit.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.exit_01;
            this.exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exit.Location = new System.Drawing.Point(349, 279);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(162, 75);
            this.exit.TabIndex = 4;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // joinroom
            // 
            this.joinroom.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.joinroom_01;
            this.joinroom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.joinroom.Location = new System.Drawing.Point(101, 202);
            this.joinroom.Name = "joinroom";
            this.joinroom.Size = new System.Drawing.Size(161, 75);
            this.joinroom.TabIndex = 5;
            this.joinroom.UseVisualStyleBackColor = true;
            this.joinroom.Click += new System.EventHandler(this.joinroom_Click);
            // 
            // mystatus
            // 
            this.mystatus.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.myStatus_01;
            this.mystatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mystatus.Location = new System.Drawing.Point(16, 279);
            this.mystatus.Name = "mystatus";
            this.mystatus.Size = new System.Drawing.Size(164, 75);
            this.mystatus.TabIndex = 6;
            this.mystatus.UseVisualStyleBackColor = true;
            this.mystatus.Click += new System.EventHandler(this.mystatus_Click);
            // 
            // signIn
            // 
            this.signIn.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.signIN_01;
            this.signIn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.signIn.Location = new System.Drawing.Point(101, 101);
            this.signIn.Name = "signIn";
            this.signIn.Size = new System.Drawing.Size(76, 100);
            this.signIn.TabIndex = 7;
            this.signIn.UseVisualStyleBackColor = true;
            this.signIn.Click += new System.EventHandler(this.signIn_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.addQuestion_02;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Location = new System.Drawing.Point(16, 202);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 75);
            this.button1.TabIndex = 8;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(523, 392);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.signIn);
            this.Controls.Add(this.mystatus);
            this.Controls.Add(this.joinroom);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.bestscore);
            this.Controls.Add(this.createroom);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.signout);
            this.DoubleBuffered = true;
            this.Name = "MainFrm";
            this.Text = "Trivia Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFrm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFrm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button signout;
        private System.Windows.Forms.Button signup;
        private System.Windows.Forms.Button createroom;
        private System.Windows.Forms.Button bestscore;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button joinroom;
        private System.Windows.Forms.Button mystatus;
        private System.Windows.Forms.Button signIn;
        private System.Windows.Forms.Button button1;



    }
}

