#include "User.h"

User::User(string name, SOCKET sock)
{
	this->_sock = sock;
	this->_username = name;
}

User::User(string name)
{
	this->_username = name;
}


Room* User::getRoom()
{
	return this->_currRoom;
}

void User::send(string data)
{
	try
	{
		Helper::sendData(this->_sock, data); //sending data to the user
	}
	catch (exception ex)
	{
		throw ex;
	}
}

void User::setGame(Game* gm)
{
	this->_currGame = gm;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomID, string roomName, int maxUsers, int questionsNo, int questionsTime)
{
	if (this->_currRoom != nullptr)
	{
		this->send("1141"); //if the user already have a room
		return false;
	}
	this->_currRoom = new Room(roomID, this, roomName, maxUsers, questionsNo, questionsTime); //creating the room
	// 114 code 
	this->send("1140");
	this->_currRoom->joinRoom(this);
	return true;
	
}

bool User::joinRoom(Room* newRoom)
{
	if (this->_currRoom != nullptr)
	{
		return false; //if the user joined a room already
	}
	if (newRoom->joinRoom(this))
	{
		this->_currRoom = newRoom;
		return true;
	}
	return false; 
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	int id = this->_currRoom->closeRoom(this);
	if (id != -1)
	{
		this->_currRoom = nullptr; //if the operation was successful
	}
	return id;
}

bool User::leaveGame()
{
	if (this->_currGame != nullptr)
	{
		return this->_currGame->leaveGame(this); //leaving the game
	}
	return false;
}

string User::getUsername()
{
	return this->_username;
}

Game* User::getGame()
{
	return this->_currGame;
}