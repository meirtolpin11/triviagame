﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication3
{
    class connection
    {
        public static string ip = "10.0.0.35";              // the ip to connect to     
        public static Int32 port = 8821;                    // the port of the server        
        public static TcpClient client;                     // the client socket 
        public static NetworkStream stream;                 // the data stream 
        public static string roomName;                      // the name of the connected room, if "" then the user is not connected to room 
        public static string roomID;                        // the id of the connected room, if "" then not connected 
        public static string questionNO;                    // the no. of questions in the current game 
        public static string questionTime;                  // the time for every question in the current game 
        private static byte[] buffer;                        // recieving buffer for the recv and send methods 
        public static string connectedUser;                 // the name of the connected user, if "" then not connected 
        public static string users;                         // the list of users in the room    
        public static string questions;                     // the list of game question, sometime also contains other messages from the server 
        public static bool connected = false;
        /**
         * the method opens a socket to the server 
         */
        public static void init()
        {
            client = new TcpClient(ip, port);           // creating tcpClient 
            stream = client.GetStream();
            buffer = new byte[4096];                    // msg buffer 
            connectedUser = "";
            connected = true;
        }

        public  static  string EncryptOrDecrypt(string text, string key)
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }

        /**
         * the method sends the "msg" string to the server 
         * @param: msg - the string to send 
         * return: none
         */
        public static void send(string msg)
        {
            buffer = System.Text.Encoding.ASCII.GetBytes(msg);
            stream.Write(buffer, 0, buffer.Length);
            
        }

        /** 
         * the method recieves the data from the server
         * return: the recieved string
         */
        public static string recv()
        {
            
            byte[] data = new byte[2048];
            string responseData;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Console.WriteLine(responseData);
            return responseData;
        }

        /** closing the connection to the server */
        public static void disconnect()
        {
            connected = false;
            connection.send("299");
            client.Close();
        }
    }
}
