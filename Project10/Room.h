#pragma once

#include <string>
#include <vector>
#include "User.h"
#include "Helper.h"

class User;

using namespace std;

class Room
{
public:
	/**
	This is the room constructor

	Input:
		int no - represents the room id
		User* admin - represents the admin of the room
		string name - represents the room name
		int maxUsers - represents the max allowed users for the room
		int questionNO - represents the number of questions
		int questionTime - represents the amount of time for each question
	Output:
		None.
	*/
	Room(int, User*, string, int, int, int);

	/**
	This function adds user to the room.

	Input:
		User* - represents the user
	Output:
		bool - true if successful, false otherwise
	*/
	bool joinRoom(User*);

	/**
	This function deletes the user from the room

	Input:
		User* - represents the user
	Output:
		None.
	*/
	void leaveRoom(User*);
	
	/**
	This function closes the room and deletes all the users that are in the room

	Input:	
		User* - represents the user
	Output:
		int - represents the status of the operation
	*/
	int closeRoom(User*);

	/**
	This function returns the users that are in the room

	Input:
		None.
	Output:
		vector<User*> - represents the users that are in the room
	*/
	vector<User*> getUsers();

	/**
	This function returns the list of the users in string representation

	Input:
		None.
	Output:
		string - represents the user list
	*/
	string getUsersListMessage();

	/**
	This function returns the number of questions in the room

	Input:
		None.
	Output:
		int - the number of questions
	*/
	int getQuestionsNo();

	/**
	This function returns the id of the room

	Input:
		None.
	Output:
		int - represents the room id
	*/
	int getId();

	/**
	This function returns the name of the room

	Input:
		None.
	Output:
		string - represents the name of the room
	*/
	string getName();

private:

	/**
	This function gets the name of the users in the room without the user* that is given
	in the paramiter.

	Input:
		vector<User*> - represents the room users
		User* - represents the user that the function should exclude.
	Output:
		string - represents the users string
	*/
	string getUsersAsString(vector<User*>, User*);
	
	/**
	This function sends a message to all the users

	Input:
		string - represents the msg
	Output:
		None.
	*/
	void sendMessage(string);
	
	/**
	This function sends a message to the room users, excluding the user
	that is given as paramiter.

	Input:
		string - represents the msg
	Output:
		None.
	*/
	void sendMessage(User*, string);
	
	vector<User*> _users;
	User* _admin;
	int _maxUsers, _questionTime, _questionNo, _id;
	string _name;
};