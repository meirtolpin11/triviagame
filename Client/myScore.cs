﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class myScore : Form
    {
        public myScore()
        {
            InitializeComponent();
        }

        private void myScore_Load(object sender, EventArgs e)
        {
            string servAns;
            string ans;
            connection.send("225");
            servAns = connection.recv();
            Console.WriteLine(servAns);
            if(servAns.Substring(0,3)=="126")
            {
                servAns = servAns.Substring(3);
                if(servAns[0]!=0) //zero games
                {
                    ans = "Number of games: " + int.Parse(servAns.Substring(0, 4));
                    servAns = servAns.Substring(4);
                    ans += "\nNumber of right answers: " + int.Parse(servAns.Substring(0, 6));
                    servAns = servAns.Substring(6);
                    ans += "\nNumber of wrong answers: " + int.Parse(servAns.Substring(0, 6));
                    servAns = servAns.Substring(6);
                    ans += "\nAverage time for answer: " + int.Parse(servAns.Substring(0, 2)) + "." + int.Parse(servAns.Substring(2, 2));
                    label1.Text = ans;
                }
            }
        }
    }
}
