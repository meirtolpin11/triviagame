#pragma once

#include <string>
#include <vector>

using namespace std;

class Question
{
public:
	/**
	Question constructor

	Input:
		int id - represents the question id
		string question - represents the question itself
		string correctAnswer - represents the correct answer
		string answer2 - represents the other answers
		string answer3 - represents the other answers
		string answer4 - represents the other answers
	Output:
		None.
	*/
	Question(int, string, string, string, string, string);

	/**
	Returns the question string

	Input:
		None
	Output:
		string - represents the question string
	*/
	string getQuestion();

	/**
	Represents the answers for the questions in a string array

	Input:
		None.
	Output:
		string* - represents a string array of the answers.
	*/
	string* getAnswers();

	/**
	This function returns the correct answer index in the answers string array

	Input:
		None.
	Output:
		int - represents the index of the correct answer
	*/
	int getCorrectAnswerIndex();

	/**
	This function returns the id of the question

	Input:
		None.
	Output:
		int - represents the question id.
	*/
	int getId();

private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;
};