#pragma once

#include "Question.h"
#include "User.h"
#include "DataBase.h"
#include <time.h>
#include <string>
#include <map>

using namespace std;

//Preventing circular reference.
class DataBase;
class User;

class Game
{
public:
	/**
	The game constructor.

	Input:
		const vector<User*>& - represents the users of the current game.
		int - represents the game id.
		DataBase& - represents the server database.
	*/
	Game(const vector<User*>&, int, DataBase&);

	/**
	Game object distructor.
	*/
	~Game();

	/**
	This function sends the first question to all the users.

	Input:
		None.
	Output:
		None.
	*/
	void sendFirstQuestion();

	/**
	This function handles the game finish event.

	Input:
		None.
	Output:
		None.
	*/
	void handleFinishGame(); 
	
	/**
	This function handles the next turn.

	Input:
		None.
	Output:
		bool - represents if there will be a next turn
			true if there is a next turn, false otherwise.
	*/
	bool handleNextTurn();	

	/**
	This function handles a given answer from a user.

	Input:
		User* user - represents the user
 		int answerNo - represents the answer number from the answers
		int time - represents the time it took for the user to answer.
	Output:
		bool - represents the status of handleNextTurn function
	*/
	bool handleAnswerFromUser(User*, int, int);

	/**
	This function safly disconnects user from the game with out effecting the rest of the users.

	Input:
		User* - represents the user.
	Output:
		bool - represents the status of the operation.
	*/
	bool leaveGame(User*);

	/**
	This function returns the game id
	*/
	int getID();

private:
	/**
	This function inserts a game record to the db

	Input:	
		None.
	Output:
		bool - represents the status of the operation, true if successful, false otherwise.
	*/
	bool insertGameToDB();

	/**
	This function inits questions from the database.

	Input:
		None.
	Output:
		None.
	*/
	void initQuestionsFromDB();

	/**
	This function sends a question to all the users.

	Input:
		None.
	Output:
		None.
	*/
	void sendQuestionToAllUsers();

	vector<Question*> _questions;
	vector<User*> _players;
	int _question_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int currentTurnAnswers;
	int _id;			
};