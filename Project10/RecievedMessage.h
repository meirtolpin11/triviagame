#pragma once

#include "iostream"
#include <WinSock2.h>
#include <Windows.h>
#include <vector>
#include <string>
#include "User.h"

using namespace std;

class RecievedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;

public:
	/**
	This is the recieved message constructor

	Input:
		SOCKET - represents the socket attached to this message.
		int - represents the recieved message code.
	Output:
		None.
	*/
	RecievedMessage(SOCKET sock, int code);

	/**
	This is the recieved message constructor

	Input:
		SOCKET - represents the socket attached to this message.
		int - represents the recieved message code.
		vector<string> - represents the values that are included with the msg.
	Output:
		None.
	*/
	RecievedMessage(SOCKET sock, int code, vector<string> value);

	/**
	This function returns the socket that attached to the msg.

	Input:
		None.
	Output:
		SOCKET - represents the socket.
	*/
	SOCKET getSock();

	/**
	This function returns the user attached to the msg

	Input:
		None
	Output:
		User* - represents the attached user to the msg
	*/
	User* getUser();

	/**
	This function sets the user that the msg is reffering to.

	Input:
		User* - represents the user.
	Output:
		None.
	*/
	void setUser(User* user);

	/**
	This function returns the msg code of the msg.

	Input:
		None.
	Output:
		int - represents the msg code.
	*/
	int getMessageCode();

	/**
	This function returns the msg values.

	Input:
		None.
	Output:
		vector<string>& - represents the msg values.
	*/
	vector<string>& getValues();
};