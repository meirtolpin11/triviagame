#pragma once

#include <string>
#include <vector>
#include "Question.h"
#include "User.h"
#include "sqlite3.h"


using namespace std;

class DataBase
{
public:

	void newQuestion(vector<string> values);


	/**
	This is the database constructor.
	*/
	DataBase();

	/**
	Database distructor
	*/
	~DataBase();

	/**
	This function checks if the username and the password are correct
	by searching in the db.

	Input:
		string username - represents the username
		string password - represents the password.
	Output:
		bool - represents the results of the search
			true if the username and the password are correct
			false otherwise.
	*/
	bool isUserAndPassMatch(string, string);

	/**
	This function checks if the given username exists in the db.

	Input:
		string username - represents the username
	Output:
		bool - true if user exists false otherwise.
	*/
	bool isUserExist(string);

	/**
	This function adds a new user to the database.

	Input:
		string username - represents the username.
		string password - represents the user's password.
		string email - represents the user's email.
	Output:
		bool - true if the adding was successful, false otherwise
	*/
	bool addNewUser(string, string, string);

	/**
	This function inits the questions for the game and inserts them randomally the
	the questions vector.

	Input:
		int - represents the number of questions the query
	Output:
		vector<Question*> - represents the questions vector for the game
	*/
	vector<Question*> initQuestions(int);

	/**
	This functions returns the 3 users that have the highest
	score in the game, with their scores.

	Input:
		None.
	Output:
		vector<string> - represents the best scores in the game
	*/
	vector<string> getBestScores();

	/**
	This function returns the personal status of the given user.

	Input:
		string username - represents the username of the user.
	Output:
		vector<string> - represents the personal status of the user that should be
			displayed in the client's screen.
	*/
	vector<string> getPersonalStatus(string);

	/**
	This function inserts new game record to the database.

	Input:
		None.
	Output:
		returns the game id of the new game. 
	*/
	int insertNewGame();

	/**
	This function updates the game status.

	Input:
		int - represents the game id.
	Output:
		bool - represents the status of the operation.
			true if successful, false otherwise.
	*/
	bool updateGameStatus(int);

	/**
	This function adds an answer record for the given user.

	Input:
		int gameID - represents the game id of the user
		string username - represents the user's username
		int questionId - represents the user's question id
		string answer - represents the user's answer
		bool isCurrect - represents the correctness of the answer 0 - incorrect, 1 - correct
		int answerTime - represents the time that passed before the user gave an answer.
	Output:
		bool - represents the status of the operation.
			true if successful, false otherwise.
	*/
	bool addAnswerToPlayer(int, string, int, string, bool, int);

	

private:
	/**
	This function counts the number of times that it has been called.

	Input:
		void* used - represents the pointer that the function gets
		int argc - represents the number of arguments that got in the query
		char** argv - represents the arguments them self
		char** azCol - represents the column name of the arguments.
	Output:
		int - represents the status of the operation, wether it was successful or not.
	*/
	static int callbackCount(void*, int, char**, char**);

	/**
	This function adds each question record that it gots to the questions vector.

	Input:
		void* used - represents the pointer that the function gets
		int argc - represents the number of arguments that got in the query
		char** argv - represents the arguments them self
		char** azCol - represents the column name of the arguments.
	Output:
		int - represents the status of the operation, wether it was successful or not.
	*/
	static int callbackQuestions(void*, int, char**, char**);

	static int callbackMax(void* used, int argc, char** argv, char** azCol);

	/**
	This function adds the records of the best scores to the string vector.

	Input:
		void* used - represents the pointer that the function gets
		int argc - represents the number of arguments that got in the query
		char** argv - represents the arguments them self
		char** azCol - represents the column name of the arguments.
	Output:
		int - represents the status of the operation, wether it was successful or not.
	*/
	static int callbackBestScores(void*, int, char**, char**);

	/**
	This function adds the records of the user's score to the string vector.

	Input:
		void* used - represents the pointer that the function gets
		int argc - represents the number of arguments that got in the query
		char** argv - represents the arguments them self
		char** azCol - represents the column name of the arguments.
	Output:
		int - represents the status of the operation, wether it was successful or not.
	*/
	static int callbackPersonalStatus(void*, int, char**, char**);
	sqlite3* _db;
};