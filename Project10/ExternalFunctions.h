#pragma once;

#include <string>

using namespace std;

class ExternalFunctions
{
public:
	/**
	This function gets the last inserted game id.

	Input:
		void* used - represents the pointer that the function gets
		int argc - represents the number of arguments that got in the query
		char** argv - represents the arguments them self
		char** azCol - represents the column name of the arguments.
	Output:
		int - represents the status of the operation, wether it was successful or not.
	*/
	static int callBackFindLastID(void* used, int argc, char** argv, char** azCol);

private:
	/**
	Unused functions that prevents the user from creating an instance of ExternalFunctions class.
	*/
	ExternalFunctions() {}
	ExternalFunctions(const ExternalFunctions& other) {}
	~ExternalFunctions() {}
};

