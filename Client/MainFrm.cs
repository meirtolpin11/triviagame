﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class MainFrm : Form
    {

       public MainFrm()
        {
            InitializeComponent();
            Form ip = new ipChanger();
            this.Hide();
            ip.ShowDialog();
            this.Show();
            connection.init();
            signIn.Enabled = true;
            signup.Enabled = true;
            exit.Enabled = true;
            joinroom.Enabled = false;
            createroom.Enabled = false;
            signout.Enabled = false;
            mystatus.Enabled = false;
            bestscore.Enabled = false;

        }
 
        private void Form1_Load(object sender, EventArgs e)
        {
            
            MaximizeBox = false;
        }





        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            connection.disconnect();
        }




        private void MainFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(connection.connected == true)
            {
                connection.disconnect();
            }
            
            Application.Exit();
        }

        private void signIn_Click(object sender, EventArgs e)
        {
            // opening the sign in form
            this.Hide();
            Form second = new signIn();                 // creating sign in form
            second.ShowDialog();                        // showing the sign in form 
            this.Show();
            // open signIn form 
            if (connection.connectedUser != "")
            {
                //label1.Text = "Hello, " + connection.connectedUser + ".";
                signout.Enabled = true;
                signIn.Enabled = false;
                exit.Enabled = true;
                createroom.Enabled = true;
                joinroom.Enabled = true;
                bestscore.Enabled = true;
                signup.Enabled = false;
                mystatus.Enabled = true;
            }
            
            
        }

        private void signout_Click(object sender, EventArgs e)
        {
            // exiting the game, signing out from the server 
            //MessageBox.Show("Under Construction");
            connection.send("201");
            signout.Enabled = false;
            signIn.Enabled = true;
            createroom.Enabled = false;
            joinroom.Enabled = false;
            bestscore.Enabled = false;
            signup.Enabled = true;
            mystatus.Enabled = false;
            connection.connectedUser = "";
        }

        private void exit_Click(object sender, EventArgs e)
        {
            connection.send("299");
            this.Close();
        }

        private void signup_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form second = new signUP();
            second.ShowDialog();
            this.Show();
            // open signIn form 
            if (connection.connectedUser != "")
            {
                // label1.Text = "Hello, " + connection.connectedUser + ".";
                signout.Enabled = true;
                signIn.Enabled = false;
                exit.Enabled = true;
                createroom.Enabled = true;
                joinroom.Enabled = true;
                bestscore.Enabled = true;
                signup.Enabled = false;
                mystatus.Enabled = true;
            }
        }

        private void createroom_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form rooms = new createRoom();
            rooms.ShowDialog();
            this.Show();
        }

        private void mystatus_Click(object sender, EventArgs e)
        {
            Form myStatus = new myScore();
            this.Hide();
            myStatus.ShowDialog();
            this.Show();
        }

        private void joinroom_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form rooms = new Rooms();
            rooms.ShowDialog();
            this.Show();
            // open room form 
        }

        private void bestscore_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form bestScore = new bestScores();
            bestScore.ShowDialog();
            this.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form temp = new AddQuestion();
            temp.ShowDialog();
            this.Show();
        }

    }
}
