﻿namespace WindowsFormsApplication3
{
    partial class AfterRoomJoin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.questionNo = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.questionTime = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnLeave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // questionNo
            // 
            this.questionNo.AutoSize = true;
            this.questionNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.questionNo.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionNo.Location = new System.Drawing.Point(220, 55);
            this.questionNo.Name = "questionNo";
            this.questionNo.Size = new System.Drawing.Size(2, 27);
            this.questionNo.TabIndex = 1;
            this.questionNo.Click += new System.EventHandler(this.questionNo_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Agency FB", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 39;
            this.listBox1.Location = new System.Drawing.Point(12, 110);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(628, 160);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // questionTime
            // 
            this.questionTime.AutoSize = true;
            this.questionTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.questionTime.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionTime.Location = new System.Drawing.Point(597, 55);
            this.questionTime.Name = "questionTime";
            this.questionTime.Size = new System.Drawing.Size(2, 27);
            this.questionTime.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.CreateButtonno1_01;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.Location = new System.Drawing.Point(12, 302);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(277, 163);
            this.button2.TabIndex = 5;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLeave
            // 
            this.btnLeave.BackColor = System.Drawing.Color.Transparent;
            this.btnLeave.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.LeaveRoomButton_01;
            this.btnLeave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLeave.ForeColor = System.Drawing.Color.Transparent;
            this.btnLeave.Location = new System.Drawing.Point(289, 302);
            this.btnLeave.Name = "btnLeave";
            this.btnLeave.Size = new System.Drawing.Size(351, 163);
            this.btnLeave.TabIndex = 0;
            this.btnLeave.UseVisualStyleBackColor = false;
            this.btnLeave.Click += new System.EventHandler(this.btnLeave_Click);
            // 
            // AfterRoomJoin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.RoomWallpaper_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(654, 477);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.questionTime);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.questionNo);
            this.Controls.Add(this.btnLeave);
            this.DoubleBuffered = true;
            this.Name = "AfterRoomJoin";
            this.Text = "RoomSecond";
            this.Load += new System.EventHandler(this.RoomSecond_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLeave;
        private System.Windows.Forms.Label questionNo;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label questionTime;
        private System.Windows.Forms.Button button2;
    }
}