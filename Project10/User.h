#pragma once

#include <WinSock2.h>
#include "Room.h"
#include "Game.h"
#include <iostream>
#include "Helper.h"

//Preventing circular reference
class Room;
class Game;

using namespace std;

class User
{
public:
	/**
	The user constructor, gets a username and a socket
	*/
	User(string, SOCKET);

	/**
	The user constructor, gets a username only
	*/
	User(string);

	/**
	Sends a string to the user

	Input:	
		string - represents the msg
	Output:
		None.
	*/
	void send(string);

	/**
	This function is the game setter
	*/
	void setGame(Game*);

	/**
	This function clears the room paramiter for the user
	*/
	void clearRoom();

	/**
	The username attribute getter
	*/
	string getUsername();

	/**
	The socket attribute getter
	*/
	SOCKET getSocket();

	/**
	The room attribute getter
	*/
	Room* getRoom();

	/**
	The game attribute getter
	*/
	Game* getGame();

	/**
	This function creates a room for the user

	Input:
		int roomID - the room id
		string roomName - the room name
		int maxUsers - max users number
		int questionsNo - number of questions
		int questionsTime - time for each question
	Output:
		bool - success or failure
	*/
	bool createRoom(int, string, int, int, int);

	/**
	Adds the user to the given room

	Input:
		Room* - the desired room
	Output:
		bool - success or failure
	*/
	bool joinRoom(Room*);

	/**
	This function makes the user leave the room

	Input:
		None
	Output:
		None.
	*/
	void leaveRoom();

	/**
	This function closes the room

	Input:
		None.
	Output:
		int - returns the room id if successful, false otherwise
	*/
	int closeRoom();

	/**
	This function makes the user leave the game

	Input:
		None.
	Output:
		bool - success or failure.
	*/
	bool leaveGame();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};