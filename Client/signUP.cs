﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class signUP : Form
    {
        public signUP()
        {
            InitializeComponent();
            this.tbPassword.PasswordChar = '*';             // setting this textBox to hiden password textBox
        }

        // "Sign Up" button click 
        private void ok_Click(object sender, EventArgs e)
        {
            string username, password, email, msg;
            // Getting the data from the textBoxes 
            username = this.tbUsername.Text;        // the username
            password = this.tbPassword.Text;        // the password 
            email = this.tbEmail.Text;              // the email
            // checking if the username is valid, if not - showing a message and closing
            if (!isUsernameValid(username)) 
            {
                MessageBox.Show("Error, Username is not valid, please Try again");
            }
            else
            {
                // checking if the password is valid, if not - showing a message and stopping the code 
                if (!isPasswordValid(password))
                {
                    MessageBox.Show("Error, Password is not valid, please Try again");
                }
                    // if the username and the password are valid - sending to the server 
                else
                {
                    // building the message 
                    msg = "203" + username.Length.ToString("00") + "" + username + "" + 
                        password.Length.ToString("00") + "" + password + "" + 
                        email.Length.ToString("00") + "" + email;
                    connection.send(msg);       // sending the message 
                    string answer = connection.recv();
                    // getting the answers from the server 
                    // connected successfully
                    if (answer == "1040")
                    {
                        MessageBox.Show("Successfully signed up");
                        this.Close();
                        //return 0;
                    }
                    // illegal password 
                    else if (answer == "1041")
                    {
                        MessageBox.Show("Illegal Password, try again");
                        //return -1;
                    }
                    // the Username already exist 
                    else if (answer == "1042")
                    {
                        MessageBox.Show("Username already exists, try again");
                        //return 1;
                    }
                    // username is illegal 
                    else if (answer == "1043")
                    {
                        MessageBox.Show("Username is illegal, try again");
                        //return 2;
                    }
                    // other 500 error 
                    else if (answer == "1044")
                    {
                        MessageBox.Show("Unexpected Error, try again");
                    }
                }
            }
            
        }

       
        bool isUsernameValid(string user)
        {
            if (user.Length > 0)
            {
                if (char.IsLower(user[0]) || char.IsUpper(user[0]))
                {
                    if (!user.Contains(" "))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        bool isPasswordValid(string password)
        {
            bool digit = false, Ucase = false, Lcase = false;
            if (password.Length >= 4)
            {
                if (!password.Contains(" "))
                {
                    for (int i = 0; i < password.Length; i++)
                    {
                        if (Char.IsUpper(password[i]))
                        {
                            digit = true;
                        }
                        if (char.IsLower(password[i]))
                        {
                            Ucase = true;
                        }
                        if (char.IsLower(password[i]))
                        {
                            Lcase = true;
                        }
                    }
                    if (digit && Ucase && Lcase)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }
}
