﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class bestScores : Form
    {
        public bestScores()
        {
            InitializeComponent();
        }

        private void bestScores_Load(object sender, EventArgs e)
        {
            int currentRead = 1;
            string currUsr;
            string currScore;
            connection.send("223");
            string answer = connection.recv();
            //string userAns = "";
            string[,] users = new string[3, 2];
            // 12412meirtolpin1100002704user00001504erez000008
            if(answer.Substring(0,3) == "124")
            {
                answer = answer.Substring(3);
                for (int i = 0; i < 3 && currentRead != 0; i++)
                {
                    currentRead = int.Parse(answer.Substring(0, 2));
                    if (currentRead != 0)
                    {
                        answer = answer.Substring(2);
                        currUsr = answer.Substring(0, currentRead);
                        answer = answer.Substring(currentRead);
                        currScore = int.Parse(answer.Substring(0, 6)).ToString();
                        answer = answer.Substring(6);
                        users[i, 0] = currUsr;
                        users[i, 1] = currScore.ToString();
                    }
                }
                this.label2.Text = "Username: " + users[0, 0] + "                score: " + users[0, 1];
                this.label3.Text = "Username: " + users[1, 0] + "                score: " + users[1, 1];
                this.label4.Text = "Username: " + users[2, 0] + "                score: " + users[2, 1];
            }
        }
    }
}
