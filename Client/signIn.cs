﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class signIn : Form
    {
        string username, password;
        public signIn()
        {
            InitializeComponent();
            this.passwordtb.PasswordChar = '*';
        }

       

        // the "next" button click  
        private void ok_Click(object sender, EventArgs e)
        {
            username = this.usernametb.Text; // getting the userName 
            if (!isUsernameValid(username))
            {
                MessageBox.Show("Error, Username is not valid, please Try again");
            }
            else
            {
                password = this.passwordtb.Text;
                if (!isPasswordValid(password))
                {
                    MessageBox.Show("Error, Password is not valid, please Try again");
                }
                else
                {
                    int error = connect();
                    if (error == 1)
                    {
                        MessageBox.Show("Connected successfully!");
                        //Need to set the connected user, maybe via connection class?
                        connection.connectedUser = username;
                        this.Close();
                    }
                    else if (error == -1)
                    {
                        MessageBox.Show("Wrong Username or Password");
                    }
                    else if (error == 0)
                    {
                        MessageBox.Show("User already connected, ERROR !");
                    }
                }
            }
                   
        }
 

        private int connect()
        {
            string msg;
            // building the message to the server ("Sign in message", code 200)
            msg = "200" + this.username.Length.ToString("00") + "" + this.username + "" + this.password.Length.ToString("00") + "" + this.password;
            connection.send(msg);                           // sending the message 
            // recieving msg 
            string answer = connection.recv();  
            // the user connected successfully
            if(answer == "1020")                
            {
                return 1;
            }
            // wrong password or username
            else if(answer == "1021")
            {
                return -1;
            }
            // user already connected 
            else if(answer =="1022")
            {
                return 0;
            }
            // other error 
            return 2;
        }


        /** the function checks if the string is valid username
         * @param user: the string to check
         * return: true if valid, else false
         */
        bool isUsernameValid(string user)
        {
            if (user.Length > 0)
            {
                if (char.IsLower(user[0]) ||char.IsUpper(user[0]))
                {
                    if (!user.Contains(" "))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /** checks if the given string is a valid password
         * @param password: the password string to check
         * return: true if valid, else false
         */
        bool isPasswordValid(string password)
        {
            bool digit = false, Ucase = false, Lcase = false;
            if(password.Length >=4 )
            {
                if(!password.Contains(" "))
                {
                    for (int i = 0; i < password.Length; i++)
                    {
                        if (Char.IsUpper(password[i]))
                        {
                            digit = true;
                        }
                        if (char.IsLower(password[i]))
                        {
                            Ucase = true;
                        }
                        if (char.IsLower(password[i]))
                        {
                            Lcase = true;
                        }
                    }
                    if (digit && Ucase && Lcase)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }

        // "Enter" key handler , same as the "ok" button
        private void userPsw_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                    username = this.usernametb.Text; // getting the userName 
                    if (!isUsernameValid(username))
                    {
                        MessageBox.Show("Error, Username is not valid, please Try again");
                    }
                    else
                    {
                        password = this.passwordtb.Text;
                        if (!isPasswordValid(password))
                        {
                            MessageBox.Show("Error, Password is not valid, please Try again");
                        }
                        else
                        {
                            int error = connect();
                            if (error == 1)
                            {
                                MessageBox.Show("Connected successfully!");
                                //Need to set the connected user, maybe via connection class?
                                connection.connectedUser = username;
                                this.Close();
                            }
                            else if (error == -1)
                            {
                                MessageBox.Show("Wrong Username or Password");
                            }
                            else if (error == 0)
                            {
                                MessageBox.Show("User already connected, ERROR !");
                            }
                        }
                    }
                   
                
            }
        }

        private void userPsw_TextChanged(object sender, EventArgs e)
        {

        }

    }

   
}
