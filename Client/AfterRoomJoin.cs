﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
   
    public partial class AfterRoomJoin : Form
    {
        bool threadStop = false;
        int mode;
        //represents the mode. 0 if the user is not an admin
        // 1 if the user is the admin of the room
        public AfterRoomJoin(int mode)
        {
            // the form has 2 modes - admin mode and regular mode 
            // the admin mode has "Start Game" option
            this.mode = mode;
            InitializeComponent();
            if(mode==0)
            {
                // regular 
                button2.Visible = false;   // deleting the "Start Game" button
                button2.Enabled = false;
            }
            else
            {
                // admin
            }
        }


        private void btnLeave_Click(object sender, EventArgs e)
        {
            // stoping the "reflash" thread
            this.threadStop = true;
            // if the user is regular user - leaving the room 
            if (mode == 0)
            {
                // leaving room 
                connection.send("211");                 // sending 211 code, the answer will be recieved in the "reflash" thread 
                connection.roomName = "";               // deleting the information about the room name
                connection.roomID = "";                 // deleting the information about the room if
                if(connection.recv() != "1120")
                {
                    MessageBox.Show("Room leave was unsuccessful");
                    Application.Exit();
                }
            }
            else if(mode == 1)
            {
                // admin mode , closing the room 
                connection.send("215");                 // sending "close" request
                connection.roomName = "";               // deleting the information about the room name 
                connection.roomID = "";                 // deleting the information about the room id 
                if(connection.recv() != "116")
                {
                    MessageBox.Show("Room close was unsuccessful..");
                    Application.Exit();
                }
            }
            this.Close();
        }

        private void startGame()
        {
            this.threadStop = true; // stopping the thread 
            // starting the game 
            Form game = new TheGame();
            this.Hide();
            
            game.ShowDialog();
            this.Close();

        }

        /**
         * The reflash thread - reflashing the list of connected users in real time mode 
         */ 
        private void reflash()
        {
            string startGame;                           // the string that containg the recieved data from the server 
            while (!this.threadStop)                                // while the thread is not stopped 
            {
                if (connection.stream.DataAvailable)                // checking id if there is a data available in the stream 
                {
                    startGame = connection.recv();                  // getting the data 

                    if (this.threadStop == true)                    // stopping the thread if the thread was stopped 
                    {
                        connection.questions = startGame;           // saving the recieved string into the "connection" class
                        Console.WriteLine("stopped");               // informing us that the thread was stopped 
                        break;                                      // finnaly stopping the thread 
                    }
                    // message for starting the game 
                    if (startGame.Substring(0, 3) == "118")
                    {
                        connection.questions = startGame;           // saving the recieved data in the "connection" class 

                        this.Invoke((MethodInvoker)(() => this.startGame()));       // starting the game 
                        break;                                                      // stopping the thread 
                    }
                    // list of users update 
                    else if (startGame.Substring(0, 3) == "108")
                    {
                        connection.users = startGame;               // saving the list of users in the "connection" class 
                        string users = connection.users;            // saving the list into a local variable 
                        int no = int.Parse(users.Substring(3, 1));  // getting the no. of users 
                        users = users.Substring(4);                 // deleting the no. of users 
                        int lenght;                                     
                        this.listBox1.Invoke((MethodInvoker)(() => this.listBox1.Items.Clear()));   // clearing the listBox 
                        string name;
                        // updating again the listBox 
                        for (int i = 0; i < no; i++)
                        {
                            lenght = int.Parse(users.Substring(0, 2));      // getting the legth of the username 
                            name = users.Substring(2, lenght);              // getting the username     
                            users = users.Substring(lenght + 2);            // deleting the lenght of the name and the name 
                            this.listBox1.Invoke((MethodInvoker)(() => this.listBox1.Items.Add(name)));     // inserting to the listBox 
                        }

                    }
                    else if(startGame.Substring(0,3)=="116")
                    {
                        mode = 2;
                        this.listBox1.Items.Clear();
                        this.listBox1.Items.Add("The admin has closed the room, press \"Leave Room\" to continue.");
                    }
                }
            }
        }
        Thread reflasher;
        private void RoomSecond_Load(object sender, EventArgs e)
        {
            // inserting users list 
            this.questionNo.Text =  connection.questionNO;             // showing to the user the information about the game 
            this.questionTime.Text =  connection.questionTime;
            if (mode == 0) // regular mode 
            {
                // setting the users and adding the users to the listBox 
                string users = connection.users;
                int no = int.Parse(users.Substring(3, 1));      // getting the no. of users 
                users = users.Substring(4);                     // deleting the no. of users 
                int lenght;         
                string name;
                for (int i = 0; i < no; i++)
                {
                    lenght = int.Parse(users.Substring(0, 2));      // getting the length of the name 
                    name = users.Substring(2, lenght);              // getting the name 
                    users = users.Substring(lenght + 2);            // deleting the name and the lenght of the name 
                    this.listBox1.Items.Add(name);                  // adding the name of the user to the list box 
                }
            }
            else
            {
                // WTF
                this.listBox1.Items.Add(connection.connectedUser);
            }
            reflasher = new Thread(new ThreadStart(reflash));       // starting the reflash thread  
            reflasher.Start();
        }

        
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Handling start game connection and then its done and we will need to start redifining the project look.
            connection.send("217");

        }

        private void questionNo_Click(object sender, EventArgs e)
        {

        }
    }
}
