#include "DataBase.h"
#include "ExternalFunctions.h"
#include <sstream>
#include <iomanip>
#include <time.h>


DataBase::DataBase()
{
	int rc = sqlite3_open("trivia.db", &this->_db); //opening the desired db
	int answers = 0;
	char* zErrMsg = nullptr;
	if (rc) //checking for errors
	{
		cout << "Unable to open db: " << sqlite3_errmsg(this->_db) << endl;
		sqlite3_close(this->_db);
		throw rc;
	}
}


void DataBase::newQuestion(vector<string> values)
{
	int answers = 0;
	vector<string> used;
	char* zErrMsg = nullptr;
	int correct = atoi(values[5].c_str());
	string right = values[1 + correct];
	string max = "SELECT MAX(question_id) FROM t_questions;";
	int rc = sqlite3_exec(this->_db, max.c_str(), callbackMax, &used, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL ERROR: UNABLE TO QUERY DB " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	string s = "INSERT INTO t_questions(question, correct_ans, ans2, ans3, ans4) VALUES(\"" + values[0] + "\",\"" + values[1] + "\", \"" + values[2] + "\", \"" + values[3] + "\", \"" + values[4] + "\");";
	 rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &answers, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL ERROR: UNABLE TO QUERY DB " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}

}

DataBase::~DataBase()
{
	sqlite3_close(this->_db); //closing the db
}

bool DataBase::isUserExist(string username)
{
	int answers = 0;
	char* zErrMsg=nullptr;
	string s = "select t_users.username from t_users where t_users.username = \"" + username + "\";"; //checking if the user exist
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &answers, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL ERROR: UNABLE TO QUERY DB " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	if (answers == 1) //if the amount of answers is one, the query was successful
	{
		return true;
	}
	return false;
}

int DataBase::callbackCount(void* used, int argc, char** argv, char** azCol)
{
	(*((int*)used))++; //increasing the number of records
	return SQLITE_OK;
}


bool DataBase::addNewUser(string username, string password, string email)
{
	int answers = 0;
	string s = "INSERT INTO t_users (username, password, email) VALUES(\"" + username + "\",\"" + password + "\",\"" + email + "\");"; //adds a new user
	char* zErrMsg = nullptr;
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &answers, &zErrMsg); //executing
	if (rc != SQLITE_OK)
	{
		cout << "SQL ERROR: UNABLE TO QUERY DB " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	int answers = 0;
	char* zErrMsg = nullptr;
	string s = "select t_users.username from t_users where t_users.username = \"" + username + "\" and t_users.password = \"" + password + "\";"; //searching for specific username and password record
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &answers, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL ERROR: UNABLE TO QUERY DB " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	if (answers == 1) //if we got one answer the query was successful
	{
		return true;
	}
	return false;
}

vector<Question*> DataBase::initQuestions(int questions_no)
{
	vector<Question*>* questions = new vector<Question*>(); //initilizing a new vector
	char* zErrMsg = nullptr;
	string s = "SELECT * FROM t_questions ORDER BY RANDOM() LIMIT " + to_string(questions_no) + ";"; //limiting the number of questions
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackQuestions, questions, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to get questions" << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	return *questions; //returning the questions
}

int DataBase::insertNewGame()
{
	int gameID;
	char* zErrMsg = nullptr;
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	localtime_s(&tstruct, &now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct); //getting the start time
	string s = "INSERT INTO t_games (status, start_time, end_time) VALUES(" + to_string(0) + ",\"" + string(buf) + "\",\"\");"; //inserting
	int rc = sqlite3_exec(this->_db, s.c_str(), nullptr, nullptr, &zErrMsg); 
	if (rc != SQLITE_OK)
	{
		cout << "Unable to insert the game to the table" << endl;
		sqlite3_free(zErrMsg);
		return -1;
	}
	s = "select game_id from t_games where game_id = (SELECT MAX(game_id) from t_games);"; //getting the last game id
	rc = sqlite3_exec(this->_db, s.c_str(), ExternalFunctions::callBackFindLastID, &gameID, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to get game id from table!" << endl;
		sqlite3_free(zErrMsg);
		return -1;
	}
	cout << "Game created Successfully ID: " << gameID << endl;
	return gameID; //returning the game id
}

int DataBase::callbackQuestions(void* used, int argc, char** argv, char** azCol)
{
	((vector<Question*>*)used)->push_back(new Question(stoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5])); //builds a question
	return 0;
}

int DataBase::callbackMax(void* used, int argc, char** argv, char** azCol)
{
	((vector<string>*)used)->push_back(argv[0]); //builds a question
	return 0;
}

bool DataBase::updateGameStatus(int id)
{
	char* zErrMsg = 0;
	time_t now = time(0);
	struct tm tstruct;
	char buf[80];
	localtime_s(&tstruct, &now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct); //getting the local time
	stringstream ss;
	ss << "UPDATE t_games SET status = 1, end_time =\"" << string(buf) << "\" WHERE game_id = " << id << "; "; //updating the record
	int rc = sqlite3_exec(this->_db, ss.str().c_str(), nullptr, 0, &zErrMsg); //executing
	if (rc != SQLITE_OK)
	{
		cout << "Unable to update game status" << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameID, string username, int questionId, string answer, bool isCurrect, int answerTime)
{
	stringstream ss;
	ss << "INSERT INTO t_players_answers (game_id,username,question_id,player_answer,is_correct,answer_time) VALUES(" << gameID << ",\""
		<< username << "\"," << questionId << ",\"" << answer << "\"," << isCurrect << "," << answerTime << ");"; //adding an answer record to the player
	char* zErrMsg = 0;
	int rc = sqlite3_exec(this->_db, ss.str().c_str(), nullptr, 0, &zErrMsg); //executing
	if (rc != SQLITE_OK)
	{
		cout << "Unable to insert new answer" << endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	return true;
}

vector<string> DataBase::getBestScores()
{
	vector<string>* bestScores = new vector<string>(); //creating a string vector to hold the best scores
	string s;
	s = "select username, sum(is_correct) from t_players_answers group by username order by sum(is_correct) desc LIMIT 3;"; //limiting the number of records
	char* zErrMsg = 0;
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackBestScores, bestScores, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to query best scores" << endl;
		sqlite3_free(zErrMsg);
	}
	return *bestScores;
}

int DataBase::callbackBestScores(void* used, int argc, char** argv, char** azCol)
{
	((vector<string>*)used)->push_back(argv[0]); //adding the records
	((vector<string>*)used)->push_back(argv[1]);
	return SQLITE_OK;
}

vector<string> DataBase::getPersonalStatus(string user)
{
	int counter = 0;
	vector<string>* personalScores = new vector<string>(); //creating personal score vector
	string s;
	s = "SELECT * FROM t_players_answers where username = \"" + user + "\" group by game_id;"; //getting the number of games
	char* zErrMsg = 0;
	int rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &counter, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to do job" << endl;
		sqlite3_free(zErrMsg);
	}
	personalScores->push_back(to_string(counter)); //pushing the answer
	counter = 0;
	s = "SELECT * FROM t_players_answers where username = \"" + user + "\" and is_correct = 1;"; //checking how much correct answers
	rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &counter, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to do job" << endl;
		sqlite3_free(zErrMsg);
	}
	personalScores->push_back(to_string(counter));
	counter = 0;
	s = "SELECT * FROM t_players_answers where username = \"" + user + "\" and is_correct = 0;"; //checking how much incorrect answers
	rc = sqlite3_exec(this->_db, s.c_str(), callbackCount, &counter, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to do job" << endl;
		sqlite3_free(zErrMsg);
	}
	personalScores->push_back(to_string(counter));
	s = "SELECT avg(answer_time) FROM t_players_answers where username = \"" + user + "\";"; //checking the avg answer time
	rc = sqlite3_exec(this->_db, s.c_str(), callbackPersonalStatus, &counter, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "Unable to do job" << endl;
		sqlite3_free(zErrMsg);
	}
	if (counter < 1000) //tests to prevent errors, it makes the double to an integer in order to calculate
	{
		s = "0" + to_string(counter);
	}
	else
	{
		if (counter < 10000)
		{
			s = to_string(counter);
		}
		else
		{
			s = "9999";
		}
	}
	personalScores->push_back(s);
	return *personalScores; //returning 
}

int DataBase::callbackPersonalStatus(void* used, int argc, char** argv, char** azCol)
{
	(*((int*)used)) = (int)(stod(argv[0])*100); //making the double to an int value
	return SQLITE_OK;
}