#include "TriviaServer.h"
#include <map>
#include <sstream>
#include <iomanip>
#include <iostream>

using namespace std;

TriviaServer::TriviaServer()
{
	this->_roomIdSequence = 0;
	int iResult;
	WSADATA wsaData;
	struct addrinfo hints;

	this->_db = *(new DataBase()); //initializing db
	//Starts the server, including binding and listening to incoming connections
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		cout << "WSAStartup failed with error: " << iResult << endl;
		throw SOCKET_ERROR; //throwing an exception in case of failure.
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	
	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &_result);
	if (iResult != 0) {
		cout << "getaddrinfo failed with error: " << iResult << endl;
		WSACleanup();
		throw SOCKET_ERROR;
	}

	// Create a SOCKET for connecting to server
	this->_socket = socket(_result->ai_family, _result->ai_socktype, _result->ai_protocol);
	if (this->_socket == INVALID_SOCKET) {
		cout << "socket failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(_result);
		WSACleanup();
		throw SOCKET_ERROR;
	}
}

TriviaServer::~TriviaServer()
{
	closesocket(this->_socket);
	map<SOCKET, User*>::iterator it;
	for (it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		closesocket(it->first); //Closing the sockets of all the users
	}
	this->_roomList.clear();
	this->_connectedUsers.clear();
	WSACleanup();
	cout << "Socket Closed With Out Any Errors." << endl;
	system("PAUSE");
}

void TriviaServer::server()
{
	bindAndListen();
	thread t_handleRecievedMessages(&TriviaServer::handleRecievedMessages, this); //handling recieved messages thread started
	t_handleRecievedMessages.detach();
	while (true)
	{
		accept(); //accepting incoming clients
	}
}

void TriviaServer::bindAndListen()
{
	int iResult = ::bind(this->_socket, _result->ai_addr, (int)_result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		cout << "bind failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(_result);
		closesocket(this->_socket);
		WSACleanup();
		throw SOCKET_ERROR;
	}
	cout << "Server binded..." << endl;
	freeaddrinfo(_result);
	cout << "Socket Created Successfully on port " << DEFAULT_PORT << endl;

	iResult = listen(this->_socket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		cout << "listen failed with error: " << WSAGetLastError() << endl;
		closesocket(this->_socket);
		WSACleanup();
		throw SOCKET_ERROR;
	}
	cout << "Server listens for incoming clients..." << endl;
}

User* TriviaServer::handleSingin(RecievedMessage* msg)
{
	string name;
	
	if (msg->getMessageCode() == 200)
	{
		// checking if its a signIn message
		vector<string> data = msg->getValues();					// getting username and password
		// checking if user exist 
		if (this->_db.isUserExist(data[0]))
		{
			// is the password match 
			if (this->_db.isUserAndPassMatch(data[0], data[1]))
			{

				name = data[0];
				// checking if connected 
				if (this->getUserByName(data[0]) == nullptr) //checks if the user is connected
				{

					cout << "---------------------------------------------------------" << endl;
					cout << "Recieved Sign In request. From: " << name << endl;
					//Creating the user
					User* usr = new User(data[0], msg->getSock());
					this->_connectedUsers.insert(pair<SOCKET,User*>(msg->getSock(), usr));
					Helper::sendData(msg->getSock(), "1020");
					cout << "User: " << name << " connected successfully. " << endl;
					cout << "---------------------------------------------------------" << endl;
					return usr;
				}
				else
				{
					// user already connected.
					Helper::sendData(msg->getSock(), "1022");
					cout << "User: " << name << " is already connected." << endl;
				}
			}
			else
			{
				// wrong password
				Helper::sendData(msg->getSock(), "1021");
				cout << "User: " << data[0] << " entered Wrong password. " << endl;
			}
		}
		else
		{
			// wrong username
			Helper::sendData(msg->getSock(), "1021");
			cout << data[0] << " is invalid username." << endl;
		}
	}
	cout << "---------------------------------------------------------" << endl;
	return NULL;
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	User* usr = msg->getUser();
	cout << "---------------------------------------------------------" << endl;
	cout << "Recieved an Sign out request from: " << usr->getUsername() << endl;
	if (usr!=NULL && getUserBySocket(msg->getSock()) == usr) //searches for the connected user in the connectedUsers vector
	{
		this->_connectedUsers.erase(this->_connectedUsers.find(msg->getSock())); //erasing the user
		if (usr->getRoom() != nullptr)
		{
			handleCloseRoom(msg);
			handleLeaveRoom(msg);
		}
		if (usr->getGame() != nullptr)
		{
			handleLeaveGame(msg);
		}
	}
	cout << "---------------------------------------------------------" << endl;
}

bool TriviaServer::handleSignup(RecievedMessage* msg)
{
	cout << "---------------------------------------------------------" << endl;
	vector<string> values = msg->getValues();
	cout << "New sign up request from: " << values[0] << endl;
	if (Validator::isUsernameValid(values[0])) //checking if username valid
	{
		if (Validator::isPasswordValid(values[1])) //checking if password valid
		{
			if (!this->_db.isUserExist(values[0])) ///checking if username doesnt exists in the db
			{
				this->_db.addNewUser(values[0], values[1], values[2]);
				Helper::sendData(msg->getSock(), "1040");
				cout << "Registrated Successfully" << endl;
				cout << "---------------------------------------------------------" << endl;
				return true;
			}
			else
			{
				// used username
				Helper::sendData(msg->getSock(), "1042");
				cout << "The username is already registrated" << endl;
				cout << "---------------------------------------------------------" << endl;
				return false;
			}
		}
		else
		{
			// password error 
			Helper::sendData(msg->getSock(), "1041");
			cout << "The password is invalid." << endl;
			cout << "---------------------------------------------------------" << endl;
			return false;
		}
	}
	// username error
	Helper::sendData(msg->getSock(), "1043");
	cout << "The username is invalid. " << endl;
	cout << "---------------------------------------------------------" << endl;
	return false;
}

void TriviaServer::clientHandler(SOCKET clientSocket)
{
	cout << "---------------------------------------------------------" << endl;
	try
	{
		int code = Helper::getMessageTypeCode(clientSocket); //getting the msg code
		while (code != 299 && code != 0)
		{
			cout << "Recieved a message." << endl;
			//Take care of message
			addRecievedMessage(buildRecieveMessage(clientSocket, code)); //building and taking care
			code = Helper::getMessageTypeCode(clientSocket);
		}
		cout << "Client " << getUserBySocket(clientSocket) << " Disconnected" << endl;
		addRecievedMessage(buildRecieveMessage(clientSocket, code));
	}
	catch (exception ex)
	{
	}
	cout << "---------------------------------------------------------" << endl;
}

void TriviaServer::accept()
{
	try
	{
		SOCKET clientSocket = ::accept(this->_socket, NULL, NULL);
		if (clientSocket == INVALID_SOCKET)
		{
			cout << "accept failed with error: " << WSAGetLastError() << endl;
			WSACleanup();
			return;
		}
		thread cliHandler(&TriviaServer::clientHandler, this, clientSocket); //starting client handler
		cliHandler.detach();
	}
	catch (exception ex)
	{
	}
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET clientSocket, int msgCode)
{
	RecievedMessage* msg; 
	vector<string> params;
	int paramSize = 0;
	int paramsNumber = 0;
	string usrAns = "";
	int i = 0;
	switch (msgCode) //checking for message type
	{
	case 200:
		paramsNumber = 2; //changing the param number
		break;
	case 201:
	case 217:
	case 222:
	case 223:
	case 225:
	case 299:
	case 205:
	case 215:
	case 211:
		paramsNumber = 0;
		break;
	case 203:
		paramsNumber = 3;
		break;
	case 207:
	case 209:
		//Special Case
		params.push_back(Helper::getStringPartFromSocket(clientSocket, 4));
		break;
	case 999:
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		params.push_back(Helper::getStringPartFromSocket(clientSocket,1));
		break;
	case 213:
		//Special One
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2); //getting room size
		cout << "Size of room name: " << paramSize << endl;
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize)); //getting room name
		params.push_back(Helper::getStringPartFromSocket(clientSocket, 1));
		params.push_back(Helper::getStringPartFromSocket(clientSocket, 2));
		params.push_back(Helper::getStringPartFromSocket(clientSocket, 2));
		break;
	case 219:
		//Special Case
		usrAns = Helper::getStringPartFromSocket(clientSocket, 1);
		params.push_back(usrAns);
		params.push_back(Helper::getStringPartFromSocket(clientSocket, 2));
		break;
	default:
		cout << "Unrecognized MSG: " << msgCode << endl;
		break;
	}
	while (i < paramsNumber) //getting generic paramiters according to paramiter number
	{
		paramSize = Helper::getIntPartFromSocket(clientSocket, 2);
		cout << "Size of username: " << paramSize << endl;
		params.push_back(Helper::getStringPartFromSocket(clientSocket, paramSize));
		i++;
	}
	if (!params.empty()) //if there are paramiters
	{ 
		msg = new RecievedMessage(clientSocket, msgCode, params);
	}
	else
	{
		msg = new RecievedMessage(clientSocket, msgCode);
	}
	return msg;
}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	lock_guard<mutex> locker(this->_mtxRecievedMessages);
	this->_queRcvMessages.push(msg);
	this->_readyToHandle.notify_all();
	//Should notify the condition variable of the thread that should take care of the messages
}

void TriviaServer::handleRecievedMessages()
{
	RecievedMessage* currMsg;
	unique_lock<mutex> locker(this->_mtxRecievedMessages);
	locker.unlock(); //unlocking the mutex
	while (true)
	{
		while (!this->_queRcvMessages.empty()) //while the queue is not empty
		{
			locker.lock(); //locking to get the msg
			currMsg = this->_queRcvMessages.front(); //getting the msg
			this->_queRcvMessages.pop();
			locker.unlock(); //releasing for incoming messages
			//Handling the currMsg
			currMsg->setUser(getUserBySocket(currMsg->getSock()));
			switch (currMsg->getMessageCode())
			{
			case 200:
				handleSingin(currMsg);
				break;
			case 201:
				handleSignout(currMsg);
				break;
			case 203:
				handleSignup(currMsg);
				break;
			case 213:
				handleCreateRoom(currMsg);
				break;
			case 215:
				handleCloseRoom(currMsg);
				break;
			case 209:
				handleJoinRoom(currMsg);
				break;
			case 211:
				handleLeaveRoom(currMsg);
				break;
			case 207:
				handleGetUsersInRoom(currMsg);
				break;
			case 205:
				handleGetRooms(currMsg);
				break;
			case 217:
				handleStartGame(currMsg);
				break;
			case 219:
				handlePlayerAnswer(currMsg);
				break;
			case 222:
				handleLeaveGame(currMsg);
				break;
			case 223:
				handleGetBestScores(currMsg);
				break;
			case 225:
				handleGetPersonalStatus(currMsg);
				break;
			case 299:
				safeDeleteUser(currMsg);
				break;
			case 999:
				addToDatabase(currMsg);
				break;
			default:
				cout << "Unexecuted MSG: " << currMsg->getMessageCode() << endl;
				safeDeleteUser(currMsg);
				break;
			}
		}
		locker.lock(); //locking to wait for another trigger
		this->_readyToHandle.wait(locker);
		locker.unlock();
	}
}


void TriviaServer::addToDatabase(RecievedMessage* msg)
{
	this->_db.newQuestion(msg->getValues());
}

User* TriviaServer::getUserBySocket(SOCKET clientSocket)
{
	map<SOCKET, User*>::iterator it;
	for (it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (clientSocket == it->first)
		{
			return it->second;
		}
	}
	return nullptr; //Unknown Socket
}

User* TriviaServer::getUserByName(string name)
{
	map<SOCKET, User*>::iterator it;
	for (it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if (name == it->second->getUsername())
		{
			return it->second;
		}
	}
	return nullptr; //No such user
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	int temp = this->_roomIdSequence; //getting room id sequence
	User* admin = msg->getUser();
	if (!admin) return false; 
	vector<string> values = msg->getValues();
	if (admin->createRoom(temp + 1, values[0], 
		atoi(values[1].c_str()), atoi(values[2].c_str())
		, atoi(values[3].c_str())))
	{
		this->_roomIdSequence+=1;
		this->_roomList.insert(pair<int, Room*>(temp+1, admin->getRoom())); //inserting the room to the map
		return true;
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	Room* mine = msg->getUser()->getRoom();
	if (mine != nullptr)
	{
		// deleting the room if its found 
		if (msg->getUser()->closeRoom() != -1)
		{
			this->_roomList.erase(this->_roomList.find(mine->getId()));
			delete mine;
			return true;
		}
	}
	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	if (msg->getUser() == nullptr) return false;
	msg->getUser()->leaveRoom();
	Helper::sendData(msg->getSock(), "1120");
	return true;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	int id = atoi(msg->getValues()[0].c_str());
	Room* room = this->getRoomById(id);
	if (room == nullptr)
	{
		// no so room 
		msg->getUser()->send("1080");
	}
	else
	{
		// room was found 
		Helper::sendData(msg->getSock(), room->getUsersListMessage());
	}
}

Room* TriviaServer::getRoomById(int id)
{
	if (this->_roomList.find(id) != this->_roomList.end())
	{
		return this->_roomList[id];
	}
	return nullptr;
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	map<int, Room*>::iterator it;
	stringstream st;
	st << "106";
	st << setfill('0') << setw(4) << this->_roomList.size(); //getting room list size
	for (it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		st << setfill('0') << setw(4) << it->second->getId(); //building room message
		st << setfill('0') << setw(2) << it->second->getName().size();
		st << it->second->getName();
	}
	msg->getUser()->send(st.str());
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User* me = msg->getUser();
	if (!me) return false;
	Room* room = getRoomById(atoi(msg->getValues()[0].c_str()));
	if (!room) //if the room is nullptr
	{
		me->send("1102");
		return false;
	}
	return me->joinRoom(room);
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		if (msg->getUser() != nullptr)
		{
			handleSignout(msg);
		}
		closesocket(msg->getSock());
	}
	catch (exception x)
	{
	}
}

void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	bool foundedRoom = false;
	Game* myGame;
	User* me = msg->getUser();
	Room* myRoom = msg->getUser()->getRoom();
	if (!me) return; //if there is no user attached
	try
	{
		myGame = new Game(me->getRoom()->getUsers(), me->getRoom()->getQuestionsNo(), this->_db); //creating new game		
		map<int, Room*>::iterator it;
		if (this->_roomList.size() == 1) //if the room list is 1
		{
			if (this->_roomList.begin()->second == me->getRoom())
			{
				this->_roomList.erase(this->_roomList.begin()); //earasing the room
			}
			else
			{
				cout << "The requested room has not founded (!) PANIC" << endl;
				return;
			}
		}
		else //if the room list is bigger than one
		{
			for (it = this->_roomList.begin(); it != this->_roomList.end() && !foundedRoom; it++)
			{
				if (it->second == me->getRoom())
				{
					this->_roomList.erase(it); //erasing the room
					foundedRoom = true;
				}
			}
			if (!foundedRoom)
			{
				cout << "The requested room has not founded (!) PANIC" << endl;
				return;
			}
		}
		for each(User* usr in me->getRoom()->getUsers())
		{
			usr->setGame(myGame); //setting the game for each user
			usr->clearRoom();
		}
		delete myRoom; //deleting the allocated room
		myGame->sendFirstQuestion(); //sending the first question
	}
	catch (exception ex)
	{
		cout << "Handle Start Game failure: " << ex.what() << endl;
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	Game* myGame = msg->getUser()->getGame();
	if (myGame != NULL)
	{
		if (!myGame->handleAnswerFromUser(msg->getUser(), stoi(msg->getValues()[0]), stoi(msg->getValues()[1]))) //handling answer from the user
		{
			myGame->handleFinishGame(); //if the game is over, sending finish game messages
			delete myGame;
		}
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	if (msg->getUser() != nullptr)
	{
		if (msg->getUser()->leaveGame())
		{
			delete msg->getUser()->getGame(); //deleting the game in case of success at the leave
		}
		msg->getUser()->setGame(nullptr);
	}
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	vector<string> bestScores = this->_db.getBestScores();
	stringstream answerToUsr;
	answerToUsr << "124";
	for (int i = 0; i < (signed)bestScores.size(); i += 2)
	{
		answerToUsr << setfill('0') << setw(2) << bestScores[i].length();
		answerToUsr << bestScores[i];
		answerToUsr << setfill('0') << setw(6) << bestScores[i + 1];
	}
	if (bestScores.size() < 6) //because the vector includes also the score <(user,score)x3>
	{
		for (int i = bestScores.size(); i < 6; i += 2)
		{
			answerToUsr << "00"; //in case that there is no enough records
		}
	}
	Helper::sendData(msg->getSock(), answerToUsr.str()); //sending answer
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)
{
	vector<string> personalStatus = this->_db.getPersonalStatus(msg->getUser()->getUsername()); //getting personal status vector
	stringstream answerToUsr;
	answerToUsr << "126"; //126 protocol
	if (personalStatus[0] == "0")
	{
		answerToUsr << "0000";
	}
	else
	{
		answerToUsr << setfill('0') << setw(4) << personalStatus[0]; //building answer to the user
		answerToUsr << setfill('0') << setw(6) << personalStatus[1];
		answerToUsr << setfill('0') << setw(6) << personalStatus[2];
		answerToUsr << setfill('0') << setw(4) << personalStatus[3];
	}
	Helper::sendData(msg->getSock(), answerToUsr.str()); //sending the answer
}