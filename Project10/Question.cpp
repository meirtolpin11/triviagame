#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int pos;
	this->_id = id;
	this->_question = question;
	this->_answers[0] = ""; 
	this->_answers[1] = ""; 
	this->_answers[2] = ""; 
	this->_answers[3] = ""; 
	int counter = 0;
	while (counter<4) //randomization part
	{
		pos = rand() % 4; //switching the positions of each question
		if (this->_answers[pos] == "")
		{
			switch (counter)
			{
			case 0:
				this->_answers[pos] = correctAnswer;
				this->_correctAnswerIndex = pos;
				break;
			case 1:
				this->_answers[pos] = answer2;
				break;
			case 2:
				this->_answers[pos] = answer3;
				break;
			case 3:
				this->_answers[pos] = answer4;
				break;
			}
			counter++; //increasing counter
		}
	}
}

string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}