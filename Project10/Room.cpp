#include "Room.h"
#include <sstream>
#include <iomanip>
#include <vector>

using namespace std;

Room::Room(int no, User* admin,string name,int maxUsers,int questionNO,int questionTime)
{
	this->_id = no;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNo = questionNO;
	this->_questionTime = questionTime;
}

string Room::getUsersAsString(vector<User*> users, User* exclude)
{
	
	string data = "" ;
	for (int i = 0; i < (signed)users.size(); i++)
	{
		if (users[i] != exclude) //getting all the user's username exluding the user that is given as a paramiter
		{
			data += users[i]->getUsername();
		}
	}
	return data;
}

string Room::getUsersListMessage()
{
	stringstream temp;
	int size;
	temp << "108";
	int users = this->_users.size();
	temp << setw(1) << users;
	for (int i = 0; i < users; i++) //getting users list
	{
		size = this->_users[i]->getUsername().size(); //checking the size
		temp << setfill('0') << setw(2) << size;
		temp << this->_users[i]->getUsername(); //inserting the user
	}
	return temp.str();
}

void Room::sendMessage(User* exclude, string data)
{
	for (int i = 0; i < (signed)this->_users.size(); i++)
	{
		if (this->_users[i] != exclude) //sending message to all the user excluding the one in the paramiter
		{
			this->_users[i]->send(data);
		}
	}
}

void Room::sendMessage(string data)
{
	this->sendMessage(nullptr, data); //sending the msg
}

int Room::getId()
{
	return this->_id;
}

bool Room::joinRoom(User* user)
{
	stringstream temp;
	if (this->_maxUsers <= (signed)this->_users.size()) //in case of error
	{ 
		user->send("1101");
		// send error message 
		return false;
	}
	else
	{
		this->_users.push_back(user); //adding the user
		// send OK to user
		temp << "1100";
		temp << setfill('0') << setw(2) << this->_questionNo;
		temp << setfill('0') << setw(2) << this->_questionTime;
		user->send(temp.str()); // sending the info to the user

		// send 108 to other users 
		string msg = this->getUsersListMessage();
		this->sendMessage(msg);
		return true;
	}
}

void Room::leaveRoom(User* user)
{
	vector<User*>::iterator it = find(this->_users.begin(), this->_users.end(), user);
	this->_users.erase(it); //erasing the user
	this->sendMessage(this->getUsersListMessage()); //notifing the other users
}

int Room::closeRoom(User* user)
{
	if (this->_admin == user) //if the user is the admin of the room
	{
		this->sendMessage("116");
		for (int i = 0; i < (signed)this->_users.size(); i++)
		{
			if (this->_users[i] != user) 
			{
				this->_users[i]->clearRoom(); //clearing the room for each user
			}
		}
		return this->_id; //returning the id
	}
	return -1; 
}

string Room::getName()
{
	return this->_name;
}

vector<User*> Room::getUsers()
{
	return this->_users;
}

int Room::getQuestionsNo()
{
	return this->_questionNo;
}