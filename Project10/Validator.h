#pragma once

#include <iostream>
#include <string>

using namespace std;

class Validator
{
public:
	/**
	This function checks if the given password is valid for registration.

	Input:
		string - represents the password
	Output:
		bool - represents the result of the check.
	*/
	static bool isPasswordValid(string);

	/**
	This function checks if the given username is valid for registration.

	Input:
		string - represents the username
	Output:
		bool - represents the result of the check.
	*/
	static bool isUsernameValid(string);

private:
	/**
	Unused Functions.
	*/
	Validator();
	Validator(const Validator& other);

};