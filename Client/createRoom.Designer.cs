﻿namespace WindowsFormsApplication3
{
    partial class createRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRoomName = new System.Windows.Forms.TextBox();
            this.txtNumberOfPlayers = new System.Windows.Forms.TextBox();
            this.txtNumberOfQues = new System.Windows.Forms.TextBox();
            this.txtTimeForQues = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtRoomName
            // 
            this.txtRoomName.Location = new System.Drawing.Point(257, 159);
            this.txtRoomName.Name = "txtRoomName";
            this.txtRoomName.Size = new System.Drawing.Size(181, 20);
            this.txtRoomName.TabIndex = 1;
            // 
            // txtNumberOfPlayers
            // 
            this.txtNumberOfPlayers.Location = new System.Drawing.Point(257, 207);
            this.txtNumberOfPlayers.Name = "txtNumberOfPlayers";
            this.txtNumberOfPlayers.Size = new System.Drawing.Size(181, 20);
            this.txtNumberOfPlayers.TabIndex = 2;
            // 
            // txtNumberOfQues
            // 
            this.txtNumberOfQues.Location = new System.Drawing.Point(257, 250);
            this.txtNumberOfQues.Name = "txtNumberOfQues";
            this.txtNumberOfQues.Size = new System.Drawing.Size(181, 20);
            this.txtNumberOfQues.TabIndex = 3;
            // 
            // txtTimeForQues
            // 
            this.txtTimeForQues.Location = new System.Drawing.Point(257, 300);
            this.txtTimeForQues.Name = "txtTimeForQues";
            this.txtTimeForQues.Size = new System.Drawing.Size(181, 20);
            this.txtTimeForQues.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.createRoom_01;
            this.btnSend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSend.Location = new System.Drawing.Point(405, 326);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(190, 85);
            this.btnSend.TabIndex = 5;
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // createRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.CreateWallpaperSecond;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(607, 423);
            this.Controls.Add(this.txtRoomName);
            this.Controls.Add(this.txtTimeForQues);
            this.Controls.Add(this.txtNumberOfQues);
            this.Controls.Add(this.txtNumberOfPlayers);
            this.Controls.Add(this.btnSend);
            this.DoubleBuffered = true;
            this.Name = "createRoom";
            this.Text = "createRoom";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtRoomName;
        private System.Windows.Forms.TextBox txtNumberOfPlayers;
        private System.Windows.Forms.TextBox txtNumberOfQues;
        private System.Windows.Forms.TextBox txtTimeForQues;
    }
}