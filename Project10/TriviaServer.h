#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream> //Debugging
#include "Validator.h"
#include <thread>
#include <map>
#include "RecievedMessage.h"
#include "DataBase.h"
#include <mutex>
#include "Room.h"
#include <queue>
#include <WS2tcpip.h>
#include "User.h"
#include <condition_variable>

#pragma comment(lib, "Ws2_32.lib")

#define BUF_LEN 1024
#define DEFAULT_PORT "8820"
#define SELF_IP "0.0.0.0"

using namespace std;

class TriviaServer
{
public:
	/**
	This is the server constructor.
	*/
	TriviaServer();

	/**
	This is the server distructor.
	*/
	~TriviaServer();

	/**
	This function starts the binding of the server and accepts clients.

	Input:
		None.
	Output:
		None.
	*/
	void server();

	/**
	This function binds the server and starts listening for incoming connections

	Input:	
		None.
	Output:
		None.
	*/
	void bindAndListen();

	/**
	This function accepts an incoming connection and starts the client
	handler.

	Input:
		None.
	Output:
		None.
	*/
	void accept();

	/**
	This is the client handler, which handles a messages from the user

	Input:
		SOCKET - represents the socket of the client
	Output:
		None.
	*/
	void clientHandler(SOCKET);

	/** Mine function (meir)*/
	void addToDatabase(RecievedMessage* msg);

	/**
	This function gets the user pointer by its name

	Input:
		string - represents the username
	Output:
		User* - represents the user pointer.
	*/
	User* getUserByName(string);

	/**
	This function handles a sign in message from a client.

	Input:
		RecievedMessage* msg - represents the recieved sign in message.
	Output:
		User* - represents the user pointer if the sign in was successful
	*/
	User* handleSingin(RecievedMessage* msg);

	/**
	This function handles a sign up message from a client.

	Input:
		RecievedMessage* msg - represents the sign up message
	Output:
		bool - represents the result of the sign up process.
	*/
	bool handleSignup(RecievedMessage* msg);

	/**
	This function handles a signout message from the user.

	Input:
		RecievedMessage* msg - represents the signout message.
	Output:
		None.
	*/
	void handleSignout(RecievedMessage* msg);

	/**
	This function gets the user pointer from a given SOCKET

	Input:
		SOCKET - represents the client's socket
	Output:
		User* - represents the user pointer.
	*/
	User* getUserBySocket(SOCKET);

	/**
	This function returns the room by a given id.

	Input:
		int - represents the room id.
	Output:
		Room* - represents the room pointer.
	*/
	Room* getRoomById(int);

	/**
	This function builds the recieved message from a given socket and 
	a given message code.

	Input:
		SOCKET - represents the client socket.
		int - represents the msg code.
	Output:
		RecievedMessage* - represents the builded message.
	*/
	RecievedMessage* buildRecieveMessage(SOCKET, int);

	/**
	This function handles room join message

	Input:
		RecievedMessage* - the incoming message
	Output:
		bool - success or failure
	*/
	bool handleJoinRoom(RecievedMessage*);

	/**
	This function handles room creation message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		bool - success or failure
	*/
	bool handleCreateRoom(RecievedMessage*);

	/**
	This function handles a room close msg

	Input:
		RecievedMessage* - the incoming message
	Output:
		bool - success or failure
	*/
	bool handleCloseRoom(RecievedMessage*);

	/**
	This function handles leave room msg

	Input:
		RecievedMessage* - the incoming msg
	Output:
		bool - success or failure
	*/
	bool handleLeaveRoom(RecievedMessage*);

	/**
	This function handles get users in room message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleGetUsersInRoom(RecievedMessage*);

	/**
	This function handles get rooms message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleGetRooms(RecievedMessage*);

	/**
	This function handles start game message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleStartGame(RecievedMessage*);

	/**
	This function adds recieved message to the msg queue

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void addRecievedMessage(RecievedMessage*);

	/**
	This function handles an incoming message

	Input:
		None.
	Output:
		None.
	*/
	void handleRecievedMessages();

	/**
	This function handles a player answer message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handlePlayerAnswer(RecievedMessage*);

	/**
	This function handles leave game message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleLeaveGame(RecievedMessage*);

	/**
	This function handles get best score message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleGetBestScores(RecievedMessage*);

	/**
	This function handles get personal status message

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void handleGetPersonalStatus(RecievedMessage*);

	/**
	This function safly delete users from the server when needed

	Input:
		RecievedMessage* - the incoming msg
	Output:
		None.
	*/
	void safeDeleteUser(RecievedMessage*);
private:
	SOCKET _socket;
	DataBase _db;
	map<SOCKET, User*> _connectedUsers;
	map<int, Room*> _roomList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	int _roomIdSequence; 
	struct addrinfo* _result; 
	condition_variable _readyToHandle;
};