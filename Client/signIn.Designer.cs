﻿namespace WindowsFormsApplication3
{
    partial class signIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.passwordtb = new System.Windows.Forms.TextBox();
            this.usernametb = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // passwordtb
            // 
            this.passwordtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passwordtb.Location = new System.Drawing.Point(151, 180);
            this.passwordtb.Name = "passwordtb";
            this.passwordtb.Size = new System.Drawing.Size(225, 26);
            this.passwordtb.TabIndex = 2;
            this.passwordtb.TextChanged += new System.EventHandler(this.userPsw_TextChanged);
            this.passwordtb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.userPsw_KeyPress);
            // 
            // usernametb
            // 
            this.usernametb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.usernametb.Location = new System.Drawing.Point(151, 148);
            this.usernametb.Name = "usernametb";
            this.usernametb.Size = new System.Drawing.Size(225, 26);
            this.usernametb.TabIndex = 1;
            // 
            // ok
            // 
            this.ok.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.signInNextButton;
            this.ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ok.Location = new System.Drawing.Point(313, 226);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(79, 75);
            this.ok.TabIndex = 3;
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // signIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = global::WindowsFormsApplication3.Properties.Resources.signinWallpaper_01;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(404, 313);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.usernametb);
            this.Controls.Add(this.passwordtb);
            this.Name = "signIn";
            this.Text = "Sign IN";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Click += new System.EventHandler(this.ok_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox passwordtb;
        private System.Windows.Forms.TextBox usernametb;
        private System.Windows.Forms.Button ok;

    }
}