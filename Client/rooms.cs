﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Rooms : Form
    {
        
        public Rooms()
        {
       
            InitializeComponent();
            init_buttons();
        }

        Dictionary<string, int> rooms;               // list of rooms
        
        /** 
         * the method inits all the button on the screen
         */
        private void init_buttons()
        {
            // sending "205" code - getting list of rooms
            connection.send("205");
            string data = connection.recv();
            string name;
            data = data.Substring(3); // deleting the message no.
            int no = int.Parse(data.Substring(0, 4));       // getting the no of the rooms 
            data = data.Substring(4);                       // deleting no. of rooms
            this.rooms = new Dictionary<string,int>();                // creating the rooms list 
            int length,id;  
            for(int i=0;i<no;i++)
            {
                id = int.Parse(data.Substring(0, 4));           // getting the room id 
                data = data.Substring(4);                       // deleting the room id 
                length = int.Parse(data.Substring(0, 2));       // getting the lenght of the room name 
                data = data.Substring(2);                       // deleting the lenght of the room name 
                name = data.Substring(0, length);               // getting the room name 
                data = data.Substring(length);                  // deleting the room name 
                rooms.Add(name, id);                            // inserting the data to the dictionary     
                this.list.Items.Add(name);                      // entering the data to the listBox on the screen 
            }   
            // until here getting the names and the IDies of the rooms and inserting into a list 

        
            
        }
        private void reflash()
        {
            // initing again all the buttons 
            this.list.Items.Clear();
            init_buttons();
        }

        /** Joining the Room 
         */ 
        private void button1_Click_1(object sender, EventArgs e)
        {
            int questionNo, questionTime;
            string name = (string)this.list.SelectedItem;           // getting the name of the room to join 
            string id="";

            // getting room ID , maybe better to create a dictionary, no time for it now :( 
            if(name==null || !this.rooms.ContainsKey(name))
            {
                // the id of room not found 
                MessageBox.Show("error");

            }
            else
            {
                id = this.rooms[name].ToString();
                string msg;
                msg = "209"  + id;       // building the message 
                connection.send(msg);          // sending the message 
                string data = connection.recv();        // the recieved message 
                // the message code 
                switch(data.Substring(0,4))
                {
                        // successfully connected to the room, opening the next form 
                    case "1100":
                        questionNo = int.Parse(data.Substring(4, 2));               // getting the no. of Questions 
                        questionTime = int.Parse(data.Substring(6, 2));             // getting the time for every question
                        MessageBox.Show("successfully connected to the room :) ");  // showing connection message 
                        connection.roomID = id;                                     // saving the room id  
                        connection.roomName = name;                                 // saving the room name
                        connection.questionNO = questionNo.ToString();              // saving the no. of questions
                        connection.questionTime = questionTime.ToString();          // saving the questions time
                        connection.send("207" + id);                                // getting users 
                        string users = connection.recv();                           // getting the userList 
                        connection.users = users;
                        Form afterjoin = new AfterRoomJoin(0);                      // creating the next form               
                        this.Hide();                                                // closing this form 
                        afterjoin.ShowDialog();                                     // showing the next form             
                        this.Close();                                               // closing this form 
                        break;                                                      // exiting from the current form 
                    case "1101":
                        // the Room is full 
                        MessageBox.Show("Error, the room is full, please try again later");
                        break;
                    case "1102":
                        // the room is not Exist 
                        MessageBox.Show("Error, room not exist or other reason :( try later");
                        break;
                    default:
                        // I don't know what to do if there will appear this message box , so no comment :) 
                        MessageBox.Show("fuck"); 
                        break;
                }
                
            }
            
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            init_buttons();
        }

        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
