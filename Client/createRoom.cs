﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class createRoom : Form
    {
        public createRoom()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (txtRoomName.Text != "" && txtNumberOfQues.Text != "" && txtNumberOfPlayers.Text != "" && txtTimeForQues.Text != "")
            {
                connection.roomName = txtRoomName.Text;
                connection.questionNO = txtNumberOfQues.Text;
                connection.questionTime = txtTimeForQues.Text;
                if (txtRoomName.Text.ToString().Length > 99)
                {
                    MessageBox.Show("Room name is too long please enter a 99 chars or less name.");
                }
                else if (int.Parse(txtNumberOfPlayers.Text) < 1 || int.Parse(txtNumberOfPlayers.Text) > 9)
                {
                    MessageBox.Show("Invalid player number. should be 1-9 (including the bounds)");
                }
                else if (int.Parse(connection.questionNO) > 99 || int.Parse(connection.questionNO) < 1)
                {
                    MessageBox.Show("Invalid question number. should be 1-99");
                }
                else if (int.Parse(connection.questionTime) > 99 || int.Parse(connection.questionTime) < 1)
                {
                    MessageBox.Show("Invali question time. should be 1 - 99 seconds");
                }
                else
                {
                    string msg = "213";
                    if (connection.roomName.Length < 10)
                    {
                        msg += "0" + connection.roomName.Length.ToString();
                    }
                    else
                    {
                        msg += connection.roomName.Length.ToString();
                    }
                    msg += connection.roomName + "" + int.Parse(txtNumberOfPlayers.Text);
                    if (int.Parse(connection.questionNO) < 10)
                    {
                        msg += "0" + int.Parse(connection.questionNO);
                    }
                    else
                    {
                        msg += int.Parse(connection.questionNO).ToString();
                    }
                    if (int.Parse(connection.questionTime) < 10)
                    {
                        msg += "0" + int.Parse(connection.questionTime);
                    }
                    else
                    {
                        msg += int.Parse(connection.questionTime).ToString();
                    }
                    connection.send(msg);
                    if (connection.recv() == "1140")
                    {
                        this.Hide();
                        Form afterCreation = new AfterRoomJoin(1);
                        afterCreation.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Unable to create room. please try again later.");
                    }
                }
            }
        }
    }
}
