#include "RecievedMessage.h"

using namespace std;

RecievedMessage::RecievedMessage(SOCKET sock, int code)
{
	this->_sock = sock;
	this->_messageCode = code;
}

RecievedMessage::RecievedMessage(SOCKET sock, int code, vector<string> values)
{
	this->_sock = sock;
	this->_messageCode = code;
	this->_values = values;
}

SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}

User* RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}