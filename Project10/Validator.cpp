#include "Validator.h"

bool Validator::isPasswordValid(string pass)
{
	bool digit = false, Ucase = false, Lcase = false;
	// checking if the length is more than 4 
	if (pass.size() >= 4)
	{
		if (pass.find(' ') == pass.npos)
		{
			for (int i = 0; i < (signed)pass.size(); i++)
			{
				if (isdigit(pass[i]))
				{
					digit = true;
				}
				if (isupper(pass[i]))
				{
					Ucase = true;
				}
				if (islower(pass[i]))
				{
					Lcase = true;
				}
			}
			if (digit && Ucase && Lcase)
			{
				return true;
			}
		}
	}
	return false;
}

bool Validator::isUsernameValid(string user)
{
	if (user.size() > 0)
	{
		if (islower(user[0]) || isupper(user[0]))
		{
			if (user.find(' ') == user.npos)
			{
				return true;
			}
		}
	}
	return false;
}