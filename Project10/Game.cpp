#include "Game.h"
#include <sstream>
#include <iomanip>

using namespace std;

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db) //_db(db) triggers the copy constructor
{
	srand((unsigned int)time(NULL));							// using later for RAND function	
	this->_players = players;									// initing variables 
	this->_question_no = questionsNo;
	this->_id = this->_db.insertNewGame();      				// getting Game ID and saving it as a global variable 
	this->initQuestionsFromDB();
	int counter = 0;
	pair<string, int> currScore; //pair of score for each user
	currScore.second = 0;
	for (int i = 0; i < (signed)this->_players.size(); i++)
	{
		this->_players[i]->setGame(this); //setting the game for each player
		currScore.first = this->_players[i]->getUsername(); //setting username
		this->_results.insert(currScore); //inserting to the map
	}
}

Game::~Game()
{
	for (int i = 0; i < (signed)this->_questions.size(); i++)
	{
		delete this->_questions[i]; //deleting the questions
	}
	this->_questions.clear();
	this->_players.clear();
}

void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

void Game::sendQuestionToAllUsers()
{
	this->currentTurnAnswers = 0; //resetting the current answers									
	Question* temp = this->_questions[this->_currQuestionIndex]; //getting the current question
	stringstream s;
	s << "118";			// protocol 118
	s << setfill('0') << setw(3) << temp->getQuestion().size();
	s << temp->getQuestion();			// building the user msg 
	string* answers = temp->getAnswers();		// getting the list of answers 
	for (int i = 0; i < 4; i++)
	{
		s << setfill('0') << setw(3) << answers[i].size(); //setting the answers list
		s << answers[i];
	}
	// sending to all the users 
	for each(User* user in this->_players)
	{
		try
		{
			cout << s.str() << endl; //preview the sended message
			user->send(s.str()); //sending
		}
		catch (exception x)
		{
			// exception :(
			cout << "Exception Occured during send" << endl;
		}
	}
}

void Game::handleFinishGame()
{
	this->_db.updateGameStatus(this->_id);		// updating game status in the database 
	//string s = "121102no05"; // message code sample
	map<string, int>::iterator it;
	string finalAns;
	stringstream ans;
	ans << "121";
	ans << setw(1) << this->_results.size();
	for (it = this->_results.begin(); it != this->_results.end(); it++)
	{
		ans << setfill('0') << setw(2) << it->first.size(); //building score message
		ans << it->first;
		ans << setfill('0') << setw(2) << it->second;
	}
	finalAns = ans.str(); //getting the final answer
	for each(User* user in this->_players)			// sending to all the users 
	{
		try
		{
			user->send(finalAns); 
			user->setGame(nullptr); //setting the current game to null
		}
		catch (exception x)
		{
		}
	}
}


bool Game::handleNextTurn()
{
	if (this->_players.size() <= 0)
	{
		// no users 
		return false;
	}
	// all the players have answered the question 
	if (this->currentTurnAnswers >= (signed)this->_players.size()) //should be >= because the user might leave the game after answering the question
	{
		// the last question 
		if (this->_currQuestionIndex == this->_question_no - 1) 
		{
			// closing the game
			return false;
		}
		else
		{
			// next question 
			this->_currQuestionIndex++;
			this->sendQuestionToAllUsers();
			return true;
		}
	}
	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	this->currentTurnAnswers++;

	// if the answer is right
	if (answerNo == (this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex() + 1)) //the index of the correct ans is lower than the real answer by one.
	{
		this->_results[user->getUsername()] ++;
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(),
			this->_questions[this->_currQuestionIndex]->getId(),
			this->_questions[this->_currQuestionIndex]->getAnswers()[answerNo-1],
			true, time);
		user->send("1201");
	}

	// the time was ended 
	else if (answerNo == 5)
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(),
			this->_questions[this->_currQuestionIndex]->getId(),
			"",
			false, time);
		user->send("1200");
	}

	// wrong answer 
	else
	{
		this->_db.addAnswerToPlayer(this->_id, user->getUsername(),
			this->_questions[this->_currQuestionIndex]->getId(),
			this->_questions[this->_currQuestionIndex]->getAnswers()[answerNo-1],
			false, time);
		user->send("1200");
	}

	return this->handleNextTurn(); //checking if we can move to the next turn
}

int Game::getID()
{
	return this->_id;
}

bool Game::leaveGame(User* user)
{
	vector<User*>::iterator it;
	it = find(this->_players.begin(), this->_players.end(), user);
	if (it != this->_players.end())
	{
		this->_players.erase(it);
		this->handleNextTurn();
		return (signed)this->_players.size() == 0; //checking if there are no players anymore
	}
	return false;
}

bool Game::insertGameToDB()
{
	this->_db.insertNewGame();
	return true;
}

void Game::initQuestionsFromDB()
{
	this->_questions = this->_db.initQuestions(this->_question_no); //inits questions
}
