﻿namespace WindowsFormsApplication3
{
    partial class AddQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.question = new System.Windows.Forms.TextBox();
            this.ans1 = new System.Windows.Forms.TextBox();
            this.ans2 = new System.Windows.Forms.TextBox();
            this.ans3 = new System.Windows.Forms.TextBox();
            this.ans4 = new System.Windows.Forms.TextBox();
            this.cans1 = new System.Windows.Forms.CheckBox();
            this.cans4 = new System.Windows.Forms.CheckBox();
            this.cans3 = new System.Windows.Forms.CheckBox();
            this.cans2 = new System.Windows.Forms.CheckBox();
            this.add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Agency FB", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(97, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "New Question";
            // 
            // question
            // 
            this.question.Location = new System.Drawing.Point(12, 68);
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(268, 20);
            this.question.TabIndex = 1;
            this.question.Text = "Question";
            // 
            // ans1
            // 
            this.ans1.Location = new System.Drawing.Point(53, 116);
            this.ans1.Name = "ans1";
            this.ans1.Size = new System.Drawing.Size(227, 20);
            this.ans1.TabIndex = 2;
            this.ans1.Text = "answer";
            // 
            // ans2
            // 
            this.ans2.Location = new System.Drawing.Point(53, 143);
            this.ans2.Name = "ans2";
            this.ans2.Size = new System.Drawing.Size(227, 20);
            this.ans2.TabIndex = 3;
            this.ans2.Text = "answer";
            // 
            // ans3
            // 
            this.ans3.Location = new System.Drawing.Point(53, 170);
            this.ans3.Name = "ans3";
            this.ans3.Size = new System.Drawing.Size(227, 20);
            this.ans3.TabIndex = 4;
            this.ans3.Text = "answer";
            // 
            // ans4
            // 
            this.ans4.Location = new System.Drawing.Point(53, 197);
            this.ans4.Name = "ans4";
            this.ans4.Size = new System.Drawing.Size(227, 20);
            this.ans4.TabIndex = 5;
            this.ans4.Text = "answer";
            // 
            // cans1
            // 
            this.cans1.AutoSize = true;
            this.cans1.Location = new System.Drawing.Point(32, 119);
            this.cans1.Name = "cans1";
            this.cans1.Size = new System.Drawing.Size(15, 14);
            this.cans1.TabIndex = 6;
            this.cans1.UseVisualStyleBackColor = true;
            // 
            // cans4
            // 
            this.cans4.AutoSize = true;
            this.cans4.Location = new System.Drawing.Point(32, 200);
            this.cans4.Name = "cans4";
            this.cans4.Size = new System.Drawing.Size(15, 14);
            this.cans4.TabIndex = 7;
            this.cans4.UseVisualStyleBackColor = true;
            // 
            // cans3
            // 
            this.cans3.AutoSize = true;
            this.cans3.Location = new System.Drawing.Point(32, 173);
            this.cans3.Name = "cans3";
            this.cans3.Size = new System.Drawing.Size(15, 14);
            this.cans3.TabIndex = 8;
            this.cans3.UseVisualStyleBackColor = true;
            // 
            // cans2
            // 
            this.cans2.AutoSize = true;
            this.cans2.Location = new System.Drawing.Point(32, 146);
            this.cans2.Name = "cans2";
            this.cans2.Size = new System.Drawing.Size(15, 14);
            this.cans2.TabIndex = 9;
            this.cans2.UseVisualStyleBackColor = true;
            // 
            // add
            // 
            this.add.Font = new System.Drawing.Font("Agency FB", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.add.Location = new System.Drawing.Point(205, 223);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(75, 32);
            this.add.TabIndex = 10;
            this.add.Text = "Add";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // AddQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 262);
            this.Controls.Add(this.add);
            this.Controls.Add(this.cans2);
            this.Controls.Add(this.cans3);
            this.Controls.Add(this.cans4);
            this.Controls.Add(this.cans1);
            this.Controls.Add(this.ans4);
            this.Controls.Add(this.ans3);
            this.Controls.Add(this.ans2);
            this.Controls.Add(this.ans1);
            this.Controls.Add(this.question);
            this.Controls.Add(this.label1);
            this.Name = "AddQuestion";
            this.Text = "AddQuestion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox question;
        private System.Windows.Forms.TextBox ans1;
        private System.Windows.Forms.TextBox ans2;
        private System.Windows.Forms.TextBox ans3;
        private System.Windows.Forms.TextBox ans4;
        private System.Windows.Forms.CheckBox cans1;
        private System.Windows.Forms.CheckBox cans4;
        private System.Windows.Forms.CheckBox cans3;
        private System.Windows.Forms.CheckBox cans2;
        private System.Windows.Forms.Button add;
    }
}