﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class TheGame : Form
    {
        public TheGame()
        {
            // end of game == 121
            InitializeComponent();
 
           
        }

        string[] question;
        Stopwatch watch;


        bool stopTime = false;

        /** the thread that updates the remaining time in real time mode */
        private void timeUpdater()
        {
            while (!stopTime)
            {
                if(this.watch.ElapsedMilliseconds > 0 && (int)this.watch.ElapsedMilliseconds < int.Parse(connection.questionTime )* 1000)
                {
                    this.label2.Invoke((MethodInvoker)(() => this.label2.Text = "Time Left: " 
                        + (int.Parse(connection.questionTime ) - this.watch.ElapsedMilliseconds / 1000)));
         
                }
              }
        }

        //Thread reflasher;
        Thread stopwatcher;
        Thread timeUpdater1;

        /** initing all the threads and the first question */
        private void TheGame_Load(object sender, EventArgs e)
        {

            this.timer1.Interval = int.Parse(connection.questionTime)*100;
             watch = new Stopwatch();
            watch.Start();
            stopwatcher = new Thread(new ThreadStart(stopwatch));
            stopwatcher.Start();
            timeUpdater1 = new Thread(new ThreadStart(timeUpdater));
            timeUpdater1.Start();
            new_question();
        }

        bool stopWatcher=false;

        /** the question stop watch, If the time is ended sending answer "5" to the server */
        private void stopwatch()
        {
            //int miliseconds;
            while(!stopWatcher)
            {
             
                if(this.watch.ElapsedMilliseconds >= int.Parse(connection.questionTime)*1000)
                {
                    // send answer 5 
                    string answer = "219";
                    answer += 5;
                    answer += connection.questionTime;                                                  // building the message 
                    this.button1.Invoke((MethodInvoker)(() => this.button1.Enabled = false));           // closing the buttons 
                    this.button2.Invoke((MethodInvoker)(() => this.button2.Enabled = false));
                    this.button3.Invoke((MethodInvoker)(() => this.button3.Enabled = false));
                    this.button4.Invoke((MethodInvoker)(() => this.button4.Enabled = false));
                    connection.send(answer);                                                            // sending the answer 
                    this.watch.Restart();                                                               // restarting the stopwatch 
                    connection.questions = connection.recv();                                           // getting the answer 
                    connection.questions = connection.recv();                                           // getting the next message 
                    // next question 
                    if (connection.questions.Substring(0, 3) == "118")
                    {
                        new_question();
                    }
                    // end of game 
                    else if (connection.questions.Substring(0, 3) == "121")
                    {
                        this.Invoke((MethodInvoker)(() => this.Close()));
                    }
                }
            }
        }

        private void new_question()
        {
      
            string data = connection.questions;
            data= data.Substring(3);
            int lenght;
            question = new string[5];
            for (int i = 0; i < 5; i++)
            {
                lenght = int.Parse(data.Substring(0, 3));
                question[i] = data.Substring(3, lenght);
                data = data.Substring(lenght+3);
            }
            this.label1.Invoke((MethodInvoker)(() => this.label1.Text = question[0]));
            this.button1.Invoke((MethodInvoker)(() =>this.button1.Text = question[1]));
            this.button2.Invoke((MethodInvoker)(() =>this.button2.Text = question[2]));
            this.button3.Invoke((MethodInvoker)(() =>this.button3.Text = question[3]));
            this.button4.Invoke((MethodInvoker)(() =>this.button4.Text = question[4]));
            this.button1.Invoke((MethodInvoker)(() => this.button1.BackColor = SystemColors.Control));
            this.button2.Invoke((MethodInvoker)(() => this.button2.BackColor = SystemColors.Control));
            this.button3.Invoke((MethodInvoker)(() => this.button3.BackColor = SystemColors.Control));
            this.button4.Invoke((MethodInvoker)(() => this.button4.BackColor = SystemColors.Control));
            this.button1.Invoke((MethodInvoker)(() =>this.button1.Enabled = true));
            this.button2.Invoke((MethodInvoker)(() =>this.button2.Enabled = true));
            this.button3.Invoke((MethodInvoker)(() =>this.button3.Enabled = true));
            this.button4.Invoke((MethodInvoker)(() =>this.button4.Enabled = true));
            this.watch.Restart();
        }



        // [219 answerNumber TimeInSeconds]
        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int time = (int)this.watch.ElapsedMilliseconds / 100;
            this.watch.Stop();
            string tag = ((Button)sender).Tag.ToString();
            string answer = "219";
            answer += int.Parse(tag);
            answer += time;
            this.button1.Invoke((MethodInvoker)(() => this.button1.Enabled = false));           // closing the buttons 
            this.button2.Invoke((MethodInvoker)(() => this.button2.Enabled = false));
            this.button3.Invoke((MethodInvoker)(() => this.button3.Enabled = false));
            this.button4.Invoke((MethodInvoker)(() => this.button4.Enabled = false));
            connection.send(answer);                                                            // sending the answer 
            this.watch.Restart();                                                               // restarting the stopwatch 
            connection.questions = connection.recv();                                           // getting the answer 
            if (connection.questions.Substring(3, 1) == "1")                                    // if right 
            {
                ((Button)sender).Invoke((MethodInvoker)(() => ((Button)sender).BackColor=Color.Green));
            }
            // if false (in this method it will be always false)
            else
            {
                ((Button)sender).Invoke((MethodInvoker)(() => ((Button)sender).BackColor = Color.Red));
            }
            Thread next = new Thread(nextQuestion);
            next.Start();
        }

        void nextQuestion()
        {
            Thread.Sleep(1000);
            connection.questions = connection.recv();                                           // getting the next message 
            // next question 
            if (connection.questions.Substring(0, 3) == "118")
            {
                new_question();
            }
            // end of game 
            else if (connection.questions.Substring(0, 3) == "121")
            {
                this.Invoke((MethodInvoker)(() => this.Close()));
            }
        }
       
        private void TheGame_FormClosed(object sender, FormClosedEventArgs e)
        {
            string name, score;
            int nameSize;
            string data;
            connection.send("222");                                 // sending end of game message 
            // stopping the threads 
            this.stopWatcher = true;                            
            this.stopTime = true;
            data = connection.questions.Substring(3);               // deleting the message code 
            int usersNo = int.Parse(data.Substring(0, 1));  // getting the no. of users 
            data = data.Substring(1);                                       // deleting the no. of users  
            Dictionary<string, string> users = new Dictionary<string, string>();   // users dictionary                           
            for(int i=0;i<usersNo;i++)
            {
                nameSize = int.Parse(data.Substring(0, 2));
                data = data.Substring(2);
                name = data.Substring(0, nameSize);
                data = data.Substring(nameSize);
                score = data.Substring(0, 2);
                data = data.Substring(2);
                users[name] = score;
            }
            string message= "";
            for(int i=0;i<usersNo;i++)
            {
                message += " Username:";
                message += users.Keys.ToArray()[i];
                message += ", Score:";
                message += users[users.Keys.ToArray()[i]] + "\n";
            }
            MessageBox.Show(message);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

    }
}
