#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include "hex.h"
#include "md5.h"
#include <string.h>
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);
	string encryptMD5(string);

};
